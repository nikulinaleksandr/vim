" --- NERDTREE
call pathogen#infect()
syntax on
filetype plugin indent on
autocmd VimEnter * NERDTree

" --- SHOW LINE NUMBERS
set number

" --- COLOR SCHEME
set t_Co=256
colorscheme wombat256

" --- SEARCH PARAMS
set incsearch
set hlsearch
set nowrapscan
set ignorecase


" --- Включаем распознавание типов файлов и типо-специфичные плагины:
filetype on
filetype plugin on
let NERDTreeShowHidden=1

" --- TABS
set tabstop=2
set shiftwidth=2
set smarttab
set expandtab "Ставим табы пробелами
set softtabstop=4 "4 пробела в табе

" --- STYLE
set mousehide "Спрятать курсор мыши когда набираем текст
set mouse=a "Включить поддержку мыши
set termencoding=utf-8 "Кодировка терминала
set novisualbell "Не мигать
set t_vb= "Не пищать! (Опции 'не портить текст', к сожалению, нету)
"Удобное поведение backspace
set backspace=indent,eol,start whichwrap+=<,>,[,]
"Вырубаем черточки на табах
set showtabline=0
"Колоночка, чтобы показывать плюсики для скрытия блоков кода:
set foldcolumn=1
set wrap
set linebreak

"Вырубаем .swp и ~ (резервные) файлы
set nobackup
set noswapfile
set encoding=utf-8 " Кодировка файлов по умолчанию
set fileencodings=utf8,cp1251 " Возможные кодировки файлов, если файл не в unicode кодировке,
" то будет использоваться cp1251
set paste


set showtabline=2


"Автоотступ
set autoindent
